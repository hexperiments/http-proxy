# HTTP Proxy

HTTP proxy that takes a HTTP request and appends a `JSON Web Token` with the following claims:
- `iat` - Timestamp of the request as specified by the specification
- `jti`- A cryptographic nonce that should be unique
- `payload` - A json payload of the structure: `{"user": "username", "date": "todays date"}`

The JWT is signed with a secret using the `HS512` alogrithm and appened as the `x-my-jwt` header to the upstream request.

The upstream endpoint is http://postman-echo.com/ and can be configurable.


The HTTP Proxy is based on Donaled Le Simple Proxy (https://ledinhcuong99.medium.com/build-simple-proxy-server-in-python-365bda288a52) and added to it the JWT part (Line 91 - 110 in proxy.py file)

MakeFile is provided -> type **make run** and dependencies will be created or updated before running the application
```
% make run
  python3 -m venv venv
  ./venv/bin/pip install -r requirements.txt
  Requirement already satisfied: pyjwt in ./venv/lib/python3.8/site-packages (from -r requirements.txt (line 1)) (2.1.0)
  . venv/bin/activate
  ./venv/bin/python3 proxy.py

```


![image-1.png](./image-1.png)

Now the server is running, you can call POST to http://localhost:33333/post and see x-my-jwt header is returned from postman echo.

`{
    "args": {},
    "data": {},
    "files": {},
    "form": {},
    "headers": {
        "x-forwarded-proto": "http",
        "x-forwarded-port": "80",
        "host": "localhost",
        "content-length": "0",
        "x-my-jwt": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJpYXQiOjE2MjM2NzA5OTEsImp0aSI6IjI3MmFlNDhlZDZhZDRjMzNiNWY3YzQxOGVhZWM1NTM5IiwicGF5bG9hZCI6eyJ1c2VyIjoidXNlcm5hbWUiLCJkYXRlIjoiMjAyMS0wNi0xNCJ9fQ.2JBxsQXUtsqBlnOmr7jUiI-y0PWYszKxDqnhRTvk5ZvR2-5y6Btg823OQFXjMIJYJc1EKG8XDiIyAyY8zyev8A",
        "content-type": "application/json",
        "accept": "*/*",
        "accept-encoding": "gzip, deflate, br",
        "cookie": "sails.sid=s%3AUy7yXeZkJ0J-fg"
    },
    "json": null,
    "url": "http://localhost/post"
}`

![image-3.png](./image-3.png)

## TODO
- Upstream endpoint to be configurable.
- Port of Proxy to be configurable
- Docker
- Provide `/status` page with
  - time from start
  - number of requests processed
