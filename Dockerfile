# syntax=docker/dockerfile:1

FROM python:3.8-slim-buster

WORKDIR /proxyapp

COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt

COPY proxy.py proxy.py

CMD [ "python3", "proxy.py"]