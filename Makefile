# name of the virtual environment directory
VENV := venv

build: requirements.txt
	python3 -m venv $(VENV)
	./$(VENV)/bin/pip install -r requirements.txt
	. $(VENV)/bin/activate

run: build
	./$(VENV)/bin/python3 proxy.py
